'use strict'
const path = require('path')
const utils = require('./utils')
const config = require('../config')
const vueLoaderConfig = require('./vue-loader.conf')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const InsideInstance = require(path.resolve(__dirname, process.env.CONFPATH, 'conf.webpack.js'))
const modulesDirectory = InsideInstance.isFactory ? 'factory_modules' : 'node_modules'
require('babel-polyfill')

process.env.UV_THREADPOOL_SIZE = 100

function resolve (dir) {
  return path.join(__dirname, '..', dir)
}

const createLintingRule = () => ({
  test: /\.(js|vue)$/,
  loader: 'eslint-loader',
  enforce: 'pre',
  include: [InsideInstance.context, resolve('test')],
  options: {
    formatter: require('eslint-friendly-formatter'),
    emitWarning: !config.dev.showEslintErrorsInOverlay
  }
})

module.exports = {
  context: InsideInstance.context,
  entry: {
    app: ['babel-polyfill', './index.js']
  },
  output: {
    path: config.build.assetsRoot,
    filename: '[name].js',
    publicPath: process.env.NODE_ENV === 'production'
      ? config.build.assetsPublicPath
      : config.dev.assetsPublicPath
  },
  resolve: {
    extensions: ['.js', '.vue', '.json', '.scss'],
    alias: Object.assign({}, {
      'vue$': 'vue/dist/vue.esm.js',
      '~insideApp': path.resolve('.'),
      '~insideCore': path.resolve(InsideInstance.context, `${modulesDirectory}/inside-core`),
      '~instanceCustom': InsideInstance.customPath,
      '~insideDisplays': path.resolve(InsideInstance.context, `${modulesDirectory}/inside-displays`),
      '~insideLibrary': path.resolve(InsideInstance.context, `${modulesDirectory}/inside-library`),
      '~instanceConf': path.resolve(InsideInstance.context, './conf'),
      '~insidePagebuilder': path.resolve(InsideInstance.context, `${modulesDirectory}/inside-pagebuilder`),
      '~insideTests': path.resolve(InsideInstance.context, `${modulesDirectory}/inside-tests`),
      '~insideVendors': path.resolve(InsideInstance.context, `${modulesDirectory}/inside-webpack/vendors`),
      '~insideWebpack': path.resolve(InsideInstance.context, `${modulesDirectory}/inside-webpack`),
      '~moduleDirectory': path.resolve(InsideInstance.context, modulesDirectory),
      styles: path.resolve(InsideInstance.context, `${modulesDirectory}/inside-library/styles`),
      fonts: path.resolve(InsideInstance.context, `${modulesDirectory}/inside-library/assets/fonts`)
    }, InsideInstance.aliases),
    modules: [path.resolve(InsideInstance.context, './node_modules')]
  },
  module: {
    rules: [
      ...(config.dev.useEslint ? [createLintingRule()] : []),
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: vueLoaderConfig
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: [/node_modules/]
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: utils.assetsPath('img/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader',
        exclude: /img/,
        options: {
          limit: 10000,
          name: utils.assetsPath('fonts/[name].[ext]')
        }
      },
      {
        test: /\.s[a|c]ss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'resolve-url-loader', 'sass-loader?sourceMap']
        })
      }
    ]
  },
  node: {
    // prevent webpack from injecting useless setImmediate polyfill because Vue
    // source contains it (although only uses it if it's native).
    setImmediate: false,
    // prevent webpack from injecting mocks to Node native modules
    // that does not make sense for the client
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty'
  }
}
