'use strict'
const path = require('path')

/*
* Get global conf
*/
const globalsSettings = require(path.resolve(__dirname, process.env.CONFPATH, 'conf.site.js'))

module.exports = {
  NODE_ENV: '"production"',
  GLOBAL: globalsSettings,
  CONFPATH: `"${process.env.CONFPATH}"`,
  ROOTPATH: `"${path.resolve(__dirname, process.env.CONFPATH, '../')}"`
}
