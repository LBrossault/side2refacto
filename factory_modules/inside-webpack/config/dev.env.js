'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')
const path = require('path')

/*
* Get global conf
*/
const globalsSettings = require(path.resolve(__dirname, process.env.CONFPATH, 'conf.site.js'))

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  GLOBAL: globalsSettings,
  CONFPATH: `"${process.env.CONFPATH}"`,
  ROOTPATH: `"${path.resolve(__dirname, process.env.CONFPATH, '../')}"`
})
