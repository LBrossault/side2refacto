'use strict'
const merge = require('webpack-merge')
const devEnv = require('./dev.env')
const path = require('path')

const globalsSettings = require(path.resolve(__dirname, process.env.CONFPATH, 'conf.site.js'))

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  GLOBAL: globalsSettings,
  CONFPATH: `"${process.env.CONFPATH}"`,
  ROOTPATH: `"${path.resolve(__dirname, process.env.CONFPATH, '../')}"`
})
