import Vue from 'vue'
import InsideCoretpl from './template/InsideCoretpl'
import GetAppPlugin from './plugin/GetApp'

import InsideModuleBuilder from './builders/InsideModuleBuilder'
import PageBuilder from '../inside-pagebuilder'
import PluginsBuilder from './builders/PluginsBuilder'
import RouterBuilder from './builders/RouterBuilder'
import StoreBuilder from './builders/StoreBuilder'
import TranslationsBuilder from './builders/TranslationsBuilder'

import ComponentDispatcher from './dispatcher/ComponentDispatcher'

Vue.config.productionTip = false
Vue.config.devtools = true
Vue.config.performance = true

export default class InsideCore {
  constructor () {
    console.log('Inside 😎 - Maecia')

    Vue.use(GetAppPlugin, { App: this })

    this.pagebuilder = this.setPageBuilder()
    this.insideModuleBuilder = this.setInsideModuleBuilder()

    this.insideLibrary = this.insideModuleBuilder ? this.insideModuleBuilder.getInsideLibrary() : {}

    this.componentDispatcher = new ComponentDispatcher()

    this.router = this.setRouter(this.pagebuilder)
    this.store = this.setStore(this.insideLibrary.getStoreModules())
    this.translations = this.setTranslations(this.insideLibrary.getTranslations())

    this.pluginsBuilder = this.setPluginsBuilder({ ...this.insideLibrary.getPlugins(), ...this.componentDispatcher.getAllModulePlugin() }, this.store.getStore())
    this.app = this.setApp(this.router, this.store, this.graphQlBuilder, this.translations)
  }

  /*
  * Create Vue Application
  */
  setApp (router, store, graphQl, translations) {
    return new Vue({
      el: '#app',
      i18n: translations.getI18N(),
      components: { InsideCoretpl },
      router: router.getRouter(),
      store: store.getStore(),
      template: '<InsideCoretpl/>'
    })
  }

  /*
  * Create PageBuilder Class
  */
  setPageBuilder () {
    return new PageBuilder()
  }

  /*
  * Create Router for Vue Application
  */
  setRouter (pagebuilder) {
    return new RouterBuilder(pagebuilder)
  }

  /*
  * Create Store
  */
  setStore (modules) {
    return new StoreBuilder(modules)
  }

  /*
  * Create static translations
  */
  setTranslations (translations) {
    return new TranslationsBuilder(translations)
  }

  /*
  * Get all Inside Modules and create an instance for each
  */
  setInsideModuleBuilder () {
    return new InsideModuleBuilder()
  }

  /*
  * Apply all Vue plugins
  */
  setPluginsBuilder (plugins, store) {
    return new PluginsBuilder(plugins, store)
  }

  /*
  * Getters
  */
  getApp () {
    return this.app
  }

  getRouter () {
    return this.router
  }

  getStore () {
    return this.store.getStore()
  }

  getPageBuilder () {
    return this.pagebuilder
  }

  getTranslations () {
    return this.translations
  }

  getInsideModuleBuilder () {
    return this.insideModuleBuilder
  }

  getModule (module) {
    return this.componentDispatcher.getFullModule(module)
  }

  getComponent (module, display) {
    return this.componentDispatcher.getModuleDisplay(module, display)
  }
}
