import Router from 'vue-router'
import VueAnalytics from 'vue-analytics'
import Vue from 'vue'

Vue.use(Router)

export default class RouterBuilder {
  constructor (pagebuilder) {
    this.pagebuilder = pagebuilder

    /*
    * Vue router and routes
    */
    this.routes = this.buildRoutes(this.pagebuilder)
    this.router = this.buildRouter(this.routes)

    // this.ga = this.buildGA(this.router)

    /*
    * Router middleware
    */
    this.router.beforeEach((to, from, next) => {
      this.beforeRouteChange(to, from, next)
    })
  }

  /*
  * Build routes Array
  */
  buildRoutes (pagebuilder) {
    return [
      {
        path: '*',
        name: 'Page',
        component: pagebuilder.getPageBuilderComponents()['PageBuilder']
      },
      {
        path: '/login',
        name: 'Login',
        component: pagebuilder.getPageBuilderComponents()['PageBuilder'],
        props: {
          display: 'specific',
          compName: 'login-users'
        }
      },
      {
        path: '/logout',
        name: 'Logout',
        beforeEnter: () => {
          this.router.push('/login')
        },
        component: pagebuilder.getPageBuilderComponents()['PageBuilder']
      }
    ]
  }

  /*
  * Build Vue Router
  */
  buildRouter (routes) {
    return new Router({
      routes,
      mode: 'history',
      scrollBehavior () {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve({ x: 0, y: 0 })
          }, 500)
        })
      }
    })
  }

  /*
  * Build Google Analytics
  */
  buildGA (router) {
    Vue.use(VueAnalytics, {
      id: process.env.GLOBAL.GA,
      router,
      autoTracking: {
        shouldRouterUpdate (to, from) {
          return to.path !== from.path
        }
      }
    })
  }

  /*
  * Check user connection
  */
  checkUserAccess () {
    return true
  }

  /*
  * Middleware
  */
  beforeRouteChange (to, from, next) {
    next()
  }

  /*
  * Getters
  */
  getRoutes () {
    return this.routes
  }

  getRouter () {
    return this.router
  }
}
