/*
 * @Class
 *
* Build all core plugins
*/
import Vue from 'vue'

import VueResource from 'vue-resource'
import { VueResponsiveComponents } from '~insideVendors/vue-responsive-components'
import Vue2Filters from 'vue2-filters'
import VueTippy from 'vue-tippy'

export default class PluginsBuilder {
  constructor (plugins, store) {
    const allPlugins = {
      VueResource,
      VueResponsiveComponents,
      Vue2Filters,
      VueTippy,
      ...plugins
    }

    this.initPlugins(allPlugins, store)
  }

  initPlugins (plugins, store) {
    for (let key in plugins) {
      Vue.use(plugins[key], { store })

      if (key === 'VueResource') {
        this.setVueResourceOptions()
      }
    }
  }

  setVueResourceOptions () {
    Vue.http.options.root = process.env.NODE_ENV === 'development' ? process.env.GLOBAL.localURL : `${window.location.origin}/api/v1`
    Vue.http.options.emulateJSON = true
  }
}
