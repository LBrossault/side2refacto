/*
* @Class
*
* Build store with core modules
*/

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default class StoreBuilder {
  constructor (modules = {}) {
    this.store = this.setStore(modules)
  }

  setStore (modules) {
    return new Vuex.Store({
      modules: {...modules}
    })
  }

  getStore () {
    return this.store
  }
}
