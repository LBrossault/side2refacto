import Vue from 'vue'

import InsideLibrary from '../../inside-library'
import fileGetter from '../helper/fileGetter'

export default class InsideModuleBuilder {
  constructor () {
    this.insideLibrary = this.setInsideLibrary()

    this.getOverridesComponents()
  }

  /*
  * Build override components
  */
  getOverridesComponents () {
    const components = fileGetter.getOverrides()

    for (let comp in components) {
      Vue.component(comp, components[comp])
    }
  }

  /*
  * Inside Library
  */
  setInsideLibrary () {
    return new InsideLibrary()
  }
  getInsideLibrary () {
    return this.insideLibrary
  }
}
