import Vue from 'vue'
import VueI18n from 'vue-i18n'

import fileGetter from '../helper/fileGetter'

Vue.use(VueI18n)

export default class TranslationsBuilder {
  constructor (translations) {
    let coreTranslations = translations
    const customTranslations = fileGetter.getTranslations()

    /*
    * Merge custom translations
    */
    for (let lang in coreTranslations) {
      if (customTranslations[lang]) {
        coreTranslations[lang] = {
          ...coreTranslations[lang],
          ...customTranslations[lang]
        }
      }
    }

    this.I18N = new VueI18n({
      locale: process.env.GLOBAL.localLang,
      messages: coreTranslations
    })
  }

  getI18N () {
    return this.I18N
  }
}
