/**
 * @GetApp
 */

const GetApp = {}

GetApp.install = (Vue, options) => {
  Vue.prototype.getApp = () => {
    return options.App
  }
}

export default GetApp
