module.exports = {
  getOverrides: () => {
    let context = require.context(`${process.env.ROOTPATH}/inside-overrides/`, true, /\.vue/)
    let obj = {}

    context.keys().forEach((key) => {
      const file = context(key).default
      obj[file.name] = file
    })

    return obj
  },
  getTranslations: () => {
    let context = require.context(`${process.env.ROOTPATH}/inside-customs/translations/`, true, /\.json/)
    let obj = {}

    context.keys().forEach((key) => {
      let translationFile = context(key)
      obj[translationFile.lang] = translationFile.trads
    })

    return obj
  }
}
