import Voca from 'voca'
import InstanceConf from '~instanceConf/conf.module.js'

export default class ComponentDispatcher {
  constructor () {
    this.modules = {}

    for (let module in InstanceConf.modules) {
      this.modules[module] = InstanceConf.modules[module]
    }
  }

  getFullModule (module) {
    if (!module) {
      return 'error'
    }

    return this.modules[this.generateModuleName(module)].displays
  }

  getModuleDisplay (module, display) {
    if (!module || !display) {
      return 'error'
    }

    if (!this.modules[this.generateModuleName(module)]) {
      return 'error'
    }

    return this.modules[this.generateModuleName(module)].displays[display]
  }

  getModuleConfig (module) {
    return this.modules[this.generateModuleName(module)]
  }

  generateModuleName (module) {
    return `Inside${Voca.capitalize(module)}`
  }

  getAllModulePlugin () {
    const plugins = []

    for (let module in this.modules) {
      if (this.modules[module].plugin) {
        plugins[module] = this.modules[module].plugin
      }
    }

    return plugins
  }
}
