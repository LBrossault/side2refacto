export default {
  data () {
    return {
      currentData: null
    }
  },
  beforeMount () {
    this.get(`content/${this.content}`, {
      params: {
        filters: JSON.stringify({ ...this.filters, ...{ langcode: this.getCurrentLang } }),
        fields: JSON.stringify(this.fields)
      }
    }).then((res) => {
      if (res.ok) {
        this.currentData = res.body.data
      }
    })
  }
}
