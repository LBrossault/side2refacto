import Vue from 'vue'

export default {
  name: 'inside-socials',
  displays: {
    'block': Vue.component('block-socials', () => import('./displays/BlockSocials')),
    'feed': Vue.component('feed-socials', () => import('./displays/FeedSocials')),
    'shortcuts': Vue.component('shortcuts-socials', () => import('./displays/ShortcutsSocials'))
  },
  config: {
    tabs: [
      {
        name: 'content',
        label: 'Content'
      },
      {
        name: 'general',
        label: 'General'
      }
    ]
  }
}
