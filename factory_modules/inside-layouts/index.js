import Vue from 'vue'

export default {
  name: 'inside-layouts',
  displays: {
    'horizontal': Vue.component('horizontal-layouts', () => import('./displays/HorizontalLayouts')),
    'vertical': Vue.component('vertical-layouts', () => import('./displays/VerticalLayouts'))
  },
  config: {
    tabs: [{
      name: 'general',
      label: 'General'
    }]
  }
}
