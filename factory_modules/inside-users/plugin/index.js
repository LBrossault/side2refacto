/**
 * @UserPlugin
 */

import Vue from 'vue'
import VueSession from 'vue-session'

Vue.use(VueSession, {
  persist: true
})

const UserPlugin = {}

UserPlugin.install = (Vue, options) => {
  const store = options.store

  /*
  * Check user session
  */
  Vue.prototype.checkSession = (vm, route) => {
    if (!vm.$session.exists() && vm.$router) {
      vm.$router.push('/login')
    } else if (!store.getters.getUserInfos) {
      /*
      * Set infos with session data
      */
      Vue.http.headers.common['api-token'] = vm.$session.get('token')
      store.commit('setUserInfos', vm.$session.get('userInfos'))
    }

    if (route === 'Login' && vm.$session.exists()) {
      vm.$router.push('/')
    }
  }

  /*
  * Init user session
  */
  Vue.prototype.initializeSession = (vm, userInfos) => {
    vm.$session.start()
    Vue.http.headers.common['api-token'] = userInfos.api_token

    /*
    * Set user infos for the session
    */
    vm.$session.set('token', userInfos.api_token)
    vm.$session.set('userInfos', userInfos)

    /*
    * Redirect to the home
    */
    vm.$router.push('/')
  }

  /*
  * Destroy user session
  */
  Vue.prototype.destroySession = (vm) => {
    vm.$session.destroy()
    vm.$router.push('/login')
  }

  /*
  * Allow other components to use these modules
  */
  Vue.mixin({
    computed: {
      getUserInfos () {
        return this.getStore('getUserInfos')
      },
      getUserToken () {
        return this.getStore('getUserToken')
      }
    }
  })
}

export default UserPlugin
