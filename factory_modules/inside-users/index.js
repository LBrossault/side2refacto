import Vue from 'vue'

export default {
  name: 'inside-users',
  displays: {
    'author': Vue.component('author-users', () => import('./displays/AuthorUsers')),
    'header': Vue.component('header-users', () => import('./displays/HeaderUsers')),
    'login': Vue.component('login-users', () => import('./displays/LoginUsers')),
    'shortcuts': Vue.component('shortcuts-users', () => import('./displays/ShortcutsUsers'))
  },
  plugin: require('./plugin').default,
  config: {
    tabs: [
      {
        name: 'content',
        label: 'Content'
      },
      {
        name: 'general',
        label: 'General'
      }
    ]
  }
}
