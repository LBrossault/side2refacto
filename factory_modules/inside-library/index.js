import Plugins from './plugins'
import StoreModules from './store'
import Translations from './translations'

export default class InsideLibrary {
  constructor () {
    this.plugins = this.setPlugins()
    this.storeModules = this.setStoreModules()
    this.translations = this.setTranslations()
  }

  /*
  * Plugins
  */
  setPlugins () {
    return Plugins
  }
  getPlugins () {
    return this.plugins
  }

  /*
  * Store Modules
  */
  setStoreModules () {
    return StoreModules
  }
  getStoreModules () {
    return this.storeModules
  }

  /*
  * Store Modules
  */
  setTranslations () {
    return Translations
  }
  getTranslations () {
    return this.translations
  }
}
