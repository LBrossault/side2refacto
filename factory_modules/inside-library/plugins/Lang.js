/**
 * @Lang
 */

const Lang = {}

Lang.install = (Vue, options) => {
  Vue.mixin({
    computed: {
      getCurrentLang () {
        return options.store.getters.getCurrentLang
      },
      tr () {
        return (element, count) => {
          /*
          * Support pluralization
          */
          if (typeof count === 'number') {
            return this.$tc(element, count, this.getCurrentLang)
          }

          return this.$t(element, this.getCurrentLang)
        }
      }
    }
  })
}

export default Lang
