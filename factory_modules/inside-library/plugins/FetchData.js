/**
 * @FetchData
 */

const FetchData = {}
const endpoints = {
  layoutData: 'layout',
  fieldSchema: 'schema'
}

FetchData.install = (Vue) => {
  /*
  * Endpoint list
  */
  Vue.prototype.getEndpoint = (endpoint) => {
    return endpoints[endpoint]
  }

  /*
  * Get method
  */
  Vue.prototype.get = (path, params) => {
    return Vue.http.get(path, params)
      .then((response) => {
        return Promise.resolve(response)
      }, (err) => {
        return Promise.resolve(err)
      })
      .catch((error) => {
        return Promise.reject(error)
      })
  }

  /*
  * Post method
  */
  Vue.prototype.post = (path, params) => {
    return Vue.http.post(path, params)
      .then((response) => {
        return Promise.resolve(response)
      }, (error) => {
        return Promise.resolve(error)
      })
  }

  /*
  * Put method
  */
  Vue.prototype.put = (path, params) => {
    return Vue.http.put(path, params)
      .then((response) => {
        return Promise.resolve(response)
      })
      .catch((error) => {
        return Promise.reject(error)
      })
  }

  /*
  * Delete method
  */
  Vue.prototype.delete = (path, params) => {
    return Vue.http.delete(path, params)
      .then((response) => {
        return Promise.resolve(response)
      })
      .catch((error) => {
        return Promise.reject(error)
      })
  }
}

export default FetchData
