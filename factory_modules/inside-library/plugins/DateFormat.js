/**
 * @DateFormat
 */

import DateHelper from '../helpers/DateHelper'

const DateFormat = {}

DateFormat.install = (Vue, options) => {
  DateHelper.setDateLang(options.store.getters.getCurrentLang)

  Vue.prototype.setDateFormat = (date, format) => {
    return DateHelper.setDateFormat(date, format)
  }

  Vue.prototype.setDateFromNow = (date) => {
    return DateHelper.setDateFromNow(date)
  }

  Vue.prototype.setDateLang = (lang) => {
    return DateHelper.setDateLang(lang)
  }
}

export default DateFormat
