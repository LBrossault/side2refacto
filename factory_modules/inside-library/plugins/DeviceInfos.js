/**
 * @DeviceInfos
 */

const _ = require('lodash')
const bowser = require('bowser')

const DeviceInfos = {}

DeviceInfos.install = (Vue, options) => {
  const store = options.store

  setDeviceInfos()
  store.commit('setBrowser', bowser)
  store.commit('setIsTouchable', bowser.tablet || bowser.mobile)

  function setDeviceInfos () {
    const viewport = {
      width: window.innerWidth,
      height: window.innerHeight
    }

    store.commit('checkBreakpoints')
    store.commit('setViewport', viewport)
  }

  window.addEventListener('resize', _.throttle(() => {
    setDeviceInfos()
  }, 100))

  Vue.prototype.getBrowserInfos = () => {
    return `${bowser.name.toLowerCase().replace(/\s+/g, '-')}-${parseFloat(bowser.version)}`
  }

  /*
  * Useful computed to get current layout
  */
  Vue.mixin({
    computed: {
      getViewport () {
        return this.getStore('getViewport')
      },
      getSmallDesktop () {
        return this.getStore('getSmallDesktop')
      },
      getTablet () {
        return this.getStore('getTablet')
      },
      getMobile () {
        return this.getStore('getMobile')
      },
      getTouchable () {
        return this.getStore('getTouchable')
      }
    }
  })
}

export default DeviceInfos
