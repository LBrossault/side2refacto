import Blocks from './Blocks'
import DateFormat from './DateFormat'
import DeviceInfos from './DeviceInfos'
import FetchData from './FetchData'
import Lang from './Lang'
import Modal from './Modal'
import StoreManagement from './StoreManagement'

export default {
  Blocks,
  DateFormat,
  DeviceInfos,
  FetchData,
  Lang,
  Modal,
  StoreManagement
}
