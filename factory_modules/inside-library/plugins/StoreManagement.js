/**
 * @StoreManagement
 */

const StoreManagement = {}

StoreManagement.install = (Vue) => {
  Vue.mixin({
    methods: {
      /*
      * Get getters with no reactivity
      */
      getStore (key, params) {
        if (params) {
          return this.$store.getters[key](params)
        }

        return this.$store.getters[key]
      },
      /*
      * Set store
      */
      setStoreAction (label, data, action) {
        if (action) {
          this.$store.dispatch(label, data)
        } else {
          this.$store.commit(label, data)
        }
      }
    },
    computed: {
      /*
      * Get getters with reactivity
      */
      getStoreProperty (key) {
        return this.$store.getters[key]
      }
    }
  })
}

export default StoreManagement
