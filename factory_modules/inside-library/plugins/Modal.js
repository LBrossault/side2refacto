/**
 * @Modal
 */

const Modal = {}

Modal.install = (Vue, options) => {
  const store = options.store

  Vue.prototype.triggerModal = (componentName, modalData = {}, modalAttributes = {}) => {
    /*
    * Indic component to show inside
    */
    store.commit('setModalComponentName', componentName)

    /*
    * Components data
    */
    store.commit('setModalData', modalData)

    /*
    * Modal custom attributes
    */
    store.commit('setModalAttributes', modalAttributes)

    /*
    * Open modal
    */
    store.commit('setModalStatus', true)
  }

  /*
  * Close modal
  */
  Vue.prototype.closeModal = () => {
    store.commit('setModalStatus', false)
    store.commit('resetModal')
  }
}

export default Modal
