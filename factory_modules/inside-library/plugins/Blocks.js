/**
 * @Blocks
 */

const Blocks = {}

Blocks.install = (Vue) => {
  Vue.mixin({
    props: {
      'herited-data': [Array, Object],
      'herited-options': {
        type: Object,
        default: () => {
          return {}
        }
      }
    },
    computed: {
      getCurrentPath () {
        return this.getStore('getCurrentPath')
      }
    }
  })
}

export default Blocks
