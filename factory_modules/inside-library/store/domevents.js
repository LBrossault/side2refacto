/*
* State
*/
const state = {
  headerMobileState: false
}

/**
 * Getters
 */
const getters = {
  getHeaderMobileState: state => state.headerMobileState
}

/**
 * Actions
 */
const actions = {

}

/**
 * Mutations
 */
const mutations = {
  setHeaderMobileState (state, payload) {
    state.headerMobileState = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
