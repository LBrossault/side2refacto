import Vue from 'vue'
import _ from 'lodash'

function findById (obj, id) {
  let result

  for (let p in obj) {
    if (obj.id === id) {
      return obj
    } else {
      if (typeof obj[p] === 'object') {
        result = findById(obj[p], id)
        if (result) {
          return result
        }
      }
    }
  }

  return result
}

const defaultSchema = {
  'rows-0': {
    id: 'rows-0',
    layouts: {},
    layoutCounter: 0
  }
}

const state = {
  blockList: {},
  rowsSchema: _.cloneDeep(defaultSchema),
  defaultSchema,
  showEdition: true
}

/**
 * Getters
 */
const getters = {
  getDefaultSchema: state => state.defaultSchema,
  getRowsSchema: state => state.rowsSchema,
  getRowsByID: state => (id) => {
    return findById(state.rowsSchema, id)
  },
  getShowEdition () {
    return state.showEdition
  },
  getBlocks: state => state.blockList,
  getBlockByID: state => (id) => {
    return state.blockList[id]
  },
  getBlockOptions: state => (id) => {
    return state.blockList[id].blockOptions
  },
  getBlockData: state => (id) => {
    return state.blockList[id].blockData
  }
}

/**
 * Actions
 */
const actions = {}

/**
 * Mutations
 */
const mutations = {
  setShowEdition (state, payload) {
    state.showEdition = payload
  },
  initLayout (state) {
    /*
    * Reset Block List
    */
    Vue.set(state, 'blockList', {})

    /*
    * Reset Schema
    */
    state.rowsSchema = _.cloneDeep(state.defaultSchema)
  },
  /*
  * Set layout width request data
  */
  setLayoutWithData (state, payload) {
    /*
    * Set block list
    */
    Vue.set(state, 'blockList', JSON.parse(payload.blocks))

    /*
    * Set layout
    */
    Vue.set(state, 'rowsSchema', JSON.parse(payload.layout))
  },
  /*
  * Add layout to row
  */
  addRowsLayout (state, payload) {
    const parent = findById(state.rowsSchema, payload.parent)
    let rows = {}

    for (let y = 0; y < payload.columns; y++) {
      /*
      * Init rows in new layout
      */
      const rowsID = payload.id.replace('layouts', 'rows')
      rows[`${rowsID}${y}`] = {
        id: `${rowsID}${y}`,
        layouts: {},
        layoutCounter: 0
      }
    }

    Vue.set(parent.layouts, payload.id, {
      rows,
      id: payload.id,
      columns: payload.columns,
      blocks: [],
      options: payload.options
    })

    parent.layoutCounter++
  },
  /*
  * Set layout options
  */
  setLayoutOptions (state, payload) {
    const row = findById(state.rowsSchema, payload.id)

    Vue.set(row, 'options', payload.options)
    Vue.set(row, 'columns', parseInt(payload.columns))
  },
  /*
  * Delete layout with id
  */
  deleteLayout (state, payload) {
    const rowParent = findById(state.rowsSchema, payload.parentID)
    const blocks = rowParent.layouts[payload.id].blocks

    /*
    * Delete all blocks in layout
    */
    for (let y = 0; y < blocks.length; y++) {
      if (blocks[y]) {
        Vue.delete(state.blockList, blocks[y])
      }
    }

    /*
    * Delete layout
    */
    Vue.delete(rowParent.layouts, payload.id)
    rowParent.layoutCounter--
  },
  /*
  * Add Block in layout
  */
  addBlock (state, payload) {
    const rowParent = findById(state.rowsSchema, payload.parentID)

    Vue.set(state.blockList, payload.blockID, payload)
    Vue.set(rowParent.blocks, payload.blockPos, payload.blockID)
  },
  /*
  * Delete Block in layout
  */
  deleteBlock (state, payload) {
    const rowParent = findById(state.rowsSchema, payload.parentID)

    Vue.delete(state.blockList, payload.blockID)
    Vue.set(rowParent.blocks, payload.blockPos, null)
  },
  setBlockOptions (state, payload) {
    Vue.set(state.blockList[payload.blockID], 'blockOptions', payload.blockOptions)
  },
  setBlockData (state, payload) {
    Vue.set(state.blockList[payload.blockID], 'blockData', payload.blockData)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
