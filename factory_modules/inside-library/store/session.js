const Cookie = require('js-cookie')

const state = {
  userToken: null,
  userInfos: null,
  currentPath: '/',
  currentLang: Cookie.get('currentLang') ? Cookie.get('currentLang') : document.querySelector('html').getAttribute('lang') || 'en'
}

/**
 * Getters
 */
const getters = {
  getUserToken: state => state.userToken,
  getUserInfos: state => state.userInfos,
  getCurrentPath: state => state.currentPath,
  getCurrentLang: state => state.currentLang
}

/**
 * Actions
 */
const actions = {

}

/**
 * Mutations
 */
const mutations = {
  setUserToken (state, payload) {
    state.userToken = payload
  },
  setUserInfos (state, payload) {
    state.userToken = payload.api_token
    state.userInfos = Object.assign({}, payload)
  },
  setCurrentPath (state, payload) {
    state.currentPath = payload
  },
  setCurrentLang (state, payload) {
    state.currentLang = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
