import Vue from 'vue'

const state = {
  modules: {},
  fields: null,
  tables: null
}

/**
 * Getters
 */
const getters = {
  getModules: state => state.modules,
  getModule: state => (name) => {
    return state.modules[name]
  },
  getFields: state => state.fields,
  getTables: state => state.tables
}

/**
 * Actions
 */
const actions = {

}

/**
 * Mutations
 */
const mutations = {
  setModules (state, payload) {
    Vue.set(state, 'modules', payload)
  },
  setFields (state, payload) {
    const tables = {}

    for (let y = 0; y < payload.tables.length; y++) {
      tables[Object.keys(tables).length + Object.keys(payload.columns).length] = {
        COLUMN_NAME: payload.tables[y],
        DATA_TYPE: 'object'
      }
    }

    state.tables = payload.tables

    state.fields = {
      ...tables,
      ...payload.columns
    }
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
