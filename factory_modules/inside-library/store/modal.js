const state = {
  modalStatus: false,
  modalData: {},
  modalComponentName: null,
  modalAvailableComponents: {},
  modalAttributes: {}
}

/**
 * Getters
 */
const getters = {
  getModalStatus: state => state.modalStatus,
  getModalData: state => state.modalData,
  getModalComponentName: state => state.modalComponentName,
  getModalAvailableComponents: state => state.modalAvailableComponents,
  getModalAttributes: state => state.modalAttributes
}

/**
 * Actions
 */
const actions = {

}

/**
 * Mutations
 */
const mutations = {
  setModalComponentName (state, payload) {
    state.modalComponentName = payload
  },
  setModalData (state, payload) {
    state.modalData = payload
  },
  addModalAvailableComponent (state, payload) {
    state.modalAvailableComponents = Object.assign({}, state.modalAvailableComponents, payload)
  },
  setModalAttributes (state, payload) {
    state.modalAttributes = Object.assign({}, payload)
  },
  setModalStatus (state, payload) {
    state.modalStatus = payload
  },
  resetModal (state) {
    state.modalComponentName = null
    state.modalData = Object.assign({})
    state.modalAvailableComponents = Object.assign({})
    state.modalAttributes = Object.assign({})
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
