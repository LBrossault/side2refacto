const state = {
  browser: 'un browser',
  viewport: {
    width: window.innerWidth,
    height: window.innerHeight
  },
  breakpoints: {
    desktop: 1100,
    tablet: 960,
    mobile: 650
  },
  isSmallDesktop: false,
  isTablet: false,
  isMobile: false,
  isTouchable: false
}

/**
 * Getters
 */
const getters = {
  getBrowser: state => state.browser,
  getViewport: state => state.viewport,
  getSmallDesktop: state => state.isSmallDesktop,
  getTablet: state => state.isTablet,
  getMobile: state => state.isMobile,
  getTouchable: state => state.isTouchable
}

/**
 * Actions
 */
const actions = {

}

/**
 * Mutations
 */
const mutations = {
  setBrowser (state, payload) {
    state.browser = payload
  },
  setViewport (state, payload) {
    state.viewport = Object.assign({}, payload)
  },
  setIsTouchable (state, payload) {
    state.isTouchable = payload
  },
  /*
  * Check if user is on tablet or mobile
  */
  checkBreakpoints (state) {
    state.isSmallDesktop = window.innerWidth <= state.breakpoints.desktop
    state.isTablet = window.innerWidth <= state.breakpoints.tablet
    state.isMobile = window.innerWidth <= state.breakpoints.mobile
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
