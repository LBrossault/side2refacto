/*
* Date Helper
*/

import moment from 'moment'

export default {
  /*
  * Note :
  * - to add text in template : 'MMM. D, YYYY [my text] h:mm A z'
  * - to see all availables format : https://momentjs.com/docs/#/displaying/
  */
  setDateFormat: (date, format) => {
    return moment(date * 1000).format(format)
  },

  /*
  * Set Date from now
  */
  setDateFromNow: (date) => {
    return moment(date).fromNow()
  },

  /*
  * Set moment lang
  */
  setDateLang (lang) {
    return moment.locale(lang)
  }
}
