export default {
  truncateMultipleDOM (DOM, max) {
    let textLength = 0
    let indexToCut

    for (let y = 0; y < DOM.length; y++) {
      const text = DOM[y].textContent
      textLength += text.length

      if (textLength > max && !indexToCut) {
        let diff = textLength - max
        DOM[y].innerText = `${text.substr(0, diff)} ...`
      } else if (indexToCut) {
        DOM[y].style.display = 'none'
      } else {
        DOM[y].style.display = 'block'
      }
    }
  }
}
