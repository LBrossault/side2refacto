import _ from 'lodash'

export default {
  props: {
    /*
    * Field model
    * Required
    */
    model: {
      type: String,
      required: true,
      default: ''
    },
    /*
    * Field label
    * Optionnal
    */
    label: {
      type: String,
      default: ''
    },
    /*
    * Field placeholder
    * Optionnal
    */
    placeholder: {
      type: String,
      default: ''
    },
    /*
    * Field VeeValidate rules : https://vee-validate.logaretm.com/#available-rules
    * Optionnal
    */
    rules: {
      type: String,
      default: ''
    },
    /*
    * Field custom fail message - If is not defined the message will be generated by VeeValidate
    * Optionnal
    */
    failMessage: {
      type: String,
      default: ''
    },
    /*
    * Label and field display - If is not defined, it takes "block" by default
    * Optionnal
    * Values : block, inline, absolute
    */
    display: {
      type: String,
      default: 'block'
    },
    /*
    * Checkbox and Radios data
    */
    inputData: {
      type: [Array, Object],
      default: () => {
        return {}
      }
    }
  },
  data () {
    /*
    * Define dynamic model
    */
    const fieldModel = {}
    fieldModel[this.model] = this.inputData[0] ? [] : null

    return {
      fieldErrors: {},
      hasFailMessage: false,
      failMessageDefault: '',
      fieldModel,
      focused: false
    }
  },
  mounted () {
    /*
    * Check validator with initial value
    */
    this.checkValidator()

    setTimeout(() => {
      if (this.$refs.field) {
        this.disableAutoFill(this.$refs.field)
      }
    }, 500)
  },
  methods: {
    /*
    * Check if field value is correct
    */
    checkValidator () {
      this.$validator.validateAll().then(() => {
        this.fieldErrors = this.$validator.errors.items

        /*
        * Send errors to global form
        */
        this.$parent.setFormErrors(this.model, this.fieldErrors)
      })
    },
    /*
    * Tricks to disable autofill's browser
    */
    disableAutoFill (e) {
      if (e.hasAttribute('readonly')) {
        e.removeAttribute('readonly')
      }
    }
  },
  watch: {
    getModel (val) {
      this.$parent.setFormData(this.model, val)

      this.checkValidator()
    },
    /*
    * Check if field has error when user submit - block errors which pop after
    */
    showFieldsErrors () {
      if (this.getFailMessage) {
        this.hasFailMessage = true
      } else {
        this.hasFailMessage = false
      }
    },
    getFailMessage (val) {
      if (val) {
        if (val !== this.failMessageDefault && this.failMessageDefault) {
          this.hasFailMessage = false
        }

        this.failMessageDefault = val
      }
    }
  },
  computed: {
    /*
    * Return field's label
    */
    getLabel () {
      if (this.label !== '') {
        return this.label
      }

      return null
    },
    getModel () {
      return this.fieldModel[this.model]
    },
    /*
    * Get showFieldsErrors from the parent - Check if user submit form
    */
    showFieldsErrors () {
      return this.$parent.getShowFieldsErrors()
    },
    /*
    * Get the right field message
    */
    getFailMessage () {
      if (Object.keys(this.fieldErrors).length > 0) {
        const error = _.find(this.fieldErrors, (error) => {
          return error.field === [this.model][0]
        })

        return error ? error.msg : false
      }

      return false
    }
  }
}
