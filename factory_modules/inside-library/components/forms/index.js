/**
 * @file
 *
 * Index all fields type.
 */

import Vue from 'vue'

/*
* Builders
*/
export default {
  'global-form': Vue.component('global-form', () => import('./GlobalForm')),
  'input-checkbox': Vue.component('input-checkbox', () => import('./fields/InputCheckbox')),
  'input-email': Vue.component('input-email', () => import('./fields/InputEmail')),
  'input-hidden': Vue.component('input-hidden', () => import('./fields/InputHidden')),
  'input-radio': Vue.component('input-radio', () => import('./fields/InputRadio')),
  'input-password': Vue.component('input-password', () => import('./fields/InputPassword')),
  'input-phone': Vue.component('input-phone', () => import('./fields/InputPhone')),
  'input-text': Vue.component('input-text', () => import('./fields/InputText')),
  'input-textarea': Vue.component('input-textarea', () => import('./fields/InputTextarea'))
}
