/**
 * @file
 *
 * Index all fields type.
 */

import Vue from 'vue'

/*
* Builders
*/
export default {
  'default-view': Vue.component('default-view', () => import('./DefaultView'))
}
