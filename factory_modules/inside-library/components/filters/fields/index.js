/**
 * @file
 *
 * Index all filter fields type.
 */

import Vue from 'vue'

/*
* Builders
*/
export default {
  InputTextFilter: Vue.component('input-text-filter', () => import('./InputTextFilter')),
  SelectFilter: Vue.component('select-filter', () => import('./SelectFilter'))
}
