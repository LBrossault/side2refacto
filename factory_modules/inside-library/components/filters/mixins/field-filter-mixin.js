export default {
  props: {
    /*
    * Field model
    * Required
    */
    model: {
      type: String,
      required: true,
      default: ''
    },
    /*
    * Field placeholder
    * Optionnal
    */
    placeholder: {
      type: String,
      default: ''
    },
    /*
    * Checkbox and Radios data
    */
    inputData: {
      type: [Array, Object],
      default: () => {
        return {}
      }
    },
    queryPath: {
      type: String,
      default: ''
    }
  },
  data () {
    /*
    * Define dynamic model
    */
    const fieldModel = {}
    fieldModel[this.model] = null

    return {
      fieldModel,
      focused: false
    }
  },
  watch: {
    getModel (val) {
      this.$parent.setFormData(this.model, val, this.queryPath)
    }
  },
  computed: {
    getModel () {
      return this.fieldModel[this.model]
    }
  }
}
