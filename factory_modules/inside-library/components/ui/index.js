import Vue from 'vue'

export default {
  name: 'inside-ui',
  displays: {
    'bookmark': Vue.component('bookmark-ui', () => import('./BookmarkUi')),
    'button-base': Vue.component('button-base-ui', () => import('./ButtonBaseUi')),
    'hr': Vue.component('hr-ui', () => import('./HrUi')),
    'iconAction': Vue.component('icon-action-ui', () => import('./IconActionUi')),
    'image': Vue.component('image-ui', () => import('./ImageUi')),
    'langs': Vue.component('langs-ui', () => import('./LangsUi')),
    'like': Vue.component('like-ui', () => import('./LikeUi')),
    'loader': Vue.component('loader-ui', () => import('./LoaderUi'))
  },
  config: {
    tabs: [{
      name: 'general',
      label: 'General'
    }]
  }
}
