import Vue from 'vue'
import Forms from '../forms'
import Views from '../views'

export default {
  name: 'inside-common',
  displays: {
    'header': Vue.component('header-common', () => import('./HeaderCommon')),
    'menu': Vue.component('menu-common', () => import('./MenuCommon')),
    'mobile-header': Vue.component('mobile-header-common', () => import('./MobileHeaderCommon')),
    'footer': Vue.component('footer-common', () => import('./FooterCommon')),
    ...Forms,
    ...Views
  }
}
