import DateHelper from '~insideLibrary/helpers/DateHelper'

export default class HelpersTest {
  constructor (core) {
    this.core = core
  }

  setupTest () {
    describe('Date Helper', () => {
      it('setDateFormat', () => {
        expect(typeof DateHelper.setDateFormat(Date.now(), 'DD - MM - YYYY')).to.equal('string')
      })

      it('setDateFromNow', () => {
        expect(typeof DateHelper.setDateFromNow(Date.now())).to.equal('string')
      })

      it('setDateLang', () => {
        expect(typeof DateHelper.setDateLang('fr')).to.equal('string')
      })
    })
  }
}
