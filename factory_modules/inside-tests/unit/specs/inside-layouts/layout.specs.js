import Vue from 'vue'
import InsideLayouts from '~moduleDirectory/inside-layouts'

import HorizontalLayout from '~moduleDirectory/inside-layouts/displays/HorizontalLayouts'
import VerticalLayouts from '~moduleDirectory/inside-layouts/displays/VerticalLayouts'

export default class LayoutTest {
  constructor (core) {
    this.core = core
  }

  setupTest () {
    /*
  * Check config files
  */
    describe('Inside Layout', () => {
      let insideLayouts = InsideLayouts

      it('Check displays', () => {
        expect(Object.keys(insideLayouts.displays)).to.have.length.above(0)
      })

      describe('HorizontalLayouts.vue', () => {
        const Constructor = Vue.extend(HorizontalLayout)
        const _comp = new Constructor().$mount()

        it('DOM rendered', () => {
          expect(_comp.$el.classList.contains('layout')).to.equal(true)
        })
      })

      describe('VerticalLayouts.vue', () => {
        const Constructor = Vue.extend(VerticalLayouts)
        const _comp = new Constructor().$mount()

        it('DOM rendered', () => {
          expect(_comp.$el.classList.contains('layout')).to.equal(true)
        })
      })
    })
  }
}
