import Vue from 'vue'
import RowBuilder from '~insidePagebuilder/components/element-builder/RowBuilder'
import LayoutBuilder from '~insidePagebuilder/components/element-builder/LayoutBuilder'

let layouts = []
let layout = null

export default class RowBuilderTest {
  constructor (core) {
    this.core = core
  }

  setupTest () {
    describe('Row Builder', () => {
      const Extend = Vue.extend(RowBuilder)
      const vm = new Extend({
        store: this.core.getStore(),
        router: this.core.getRouter().getRouter()
      })

      vm.$mount()

      it('Check Row Edition', () => {
        vm.$store.commit('setShowEdition', true)

        expect(typeof vm.$el.querySelector('.row-builder__more')).to.equals('object')
      })

      it('Add Layout element', () => {
        const options = {
          width: {
            value: null,
            unit: null
          },
          margins: {
            top: 0,
            right: 0,
            bottom: 0,
            left: 0
          }
        }

        vm.$store.commit('addRowsLayout', {
          parent: 'rows-0',
          id: 'layouts-0-0',
          columns: 2,
          blocks: {},
          options: {
            colOptions: [options, options],
            width: {},
            maxWidth: {}
          }
        })

        vm.$nextTick(() => {
          expect(typeof vm.$el.querySelector('.row-builder__content')).to.equals('object')
        })
      })

      it('Check Row Layout length', () => {
        expect(Object.keys(vm.getRowData.layouts)).to.have.lengthOf(1)
      })

      it('Mount LayoutBuilders', () => {
        for (let _layout in vm.getRowData.layouts) {
          const ExtendLayout = Vue.extend(LayoutBuilder)
          const vmLayout = new ExtendLayout({
            store: this.core.getStore(),
            router: this.core.getRouter().getRouter(),
            propsData: {
              columns: vm.getRowData.layouts[_layout].columns,
              parentID: 'rows-0',
              depthID: 'layouts-0-0',
              heritedData: vm.getRowData.layouts[_layout]
            }
          })

          layouts.push(vmLayout)

          expect(vm.getRowData.layouts[_layout].id).to.not.equal(null)
          expect(typeof vm.getRowData.layouts[_layout]).to.equals('object')
        }

        layout = layouts[0]
      })

      it('Check Layout data', () => {
        expect(typeof layout._props.heritedData).to.equals('object')
      })

      describe('Check Layout DOM', () => {
        it('Check Layout Edition', () => {
          layout.$nextTick(() => {
            expect(typeof layout.$el.querySelector('.layout-builder__edit')).to.equals('object')
          })
        })

        it('Check Layout columns', () => {
          layout.$nextTick(() => {
            expect(this.layout.$el.querySelectorAll('.layout-builder__col')).to.have.lengthOf(2)
          })
        })

        it('Check Layout Row Builder', () => {
          layout.$nextTick(() => {
            expect(typeof this.layout.$el.querySelector('.row-builder')).to.equals('object')
          })
        })
      })
    })
  }
}
