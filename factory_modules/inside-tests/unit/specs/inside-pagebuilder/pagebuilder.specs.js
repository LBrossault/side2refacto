import Vue from 'vue'

import PageBuilder from '~insidePagebuilder'
import PageBuilderComponent from '~insidePagebuilder/components/PageBuilder'

export default class PageBuilderTest {
  constructor (core) {
    this.core = core
  }

  setupTest () {
    describe('Inside Page Builder', () => {
      const pageBuilder = new PageBuilder()

      const Extend = Vue.extend(PageBuilderComponent)
      const vm = new Extend({
        store: this.core.getStore(),
        router: this.core.getRouter().getRouter()
      })

      vm.$mount()

      it('Get Page Builder component', () => {
        expect(typeof pageBuilder.getPageBuilderComponents()).to.equals('object')
      })

      it('Check default data', () => {
        expect(vm.currentLayout).to.equals(`${process.env.GLOBAL.layout}-layouts`)
        expect(vm.isLoading).to.equals(true)
        expect(vm.layoutFailed).to.equals(false)
      })

      it('Check Loading element', () => {
        expect(typeof vm.$el.querySelector('.page-builder__loader')).to.equals('object')
      })

      it('Check Layout page', () => {
        vm.isLoading = false
        vm.currentLayout = 'horizontal-layouts'

        expect(typeof vm.$el.querySelector('.menu-common')).to.equals('object')
        expect(typeof vm.$el.querySelector('.row-builder--rows-0')).to.equals('object')
        expect(typeof vm.$el.querySelector('.footer-common')).to.equals('object')
      })

      it('Check no Layout', () => {
        vm.isLoading = false
        vm.layoutFailed = true

        expect(typeof vm.$el.querySelector('.error-page')).to.equals('object')
      })
    })
  }
}
