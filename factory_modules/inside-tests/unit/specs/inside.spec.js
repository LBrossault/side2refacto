require('es6-promise').polyfill()

import InsideCore from '~insideCore'

import CoreTest from './inside-core/core.specs.js'
import LayoutTest from './inside-layouts/layout.specs'

import HelperTest from './inside-library/helpers/date-helpers.specs'

import PageBuilderTest from './inside-pagebuilder/pagebuilder.specs'
import RowBuilderTest from './inside-pagebuilder/rowbuilder.specs'

/*
* Add app
*/
document.querySelector('body').innerHTML = `<div id="app"></div>`

/*
* Init all tests
*/
const core = new InsideCore()

const coreTest = new CoreTest(core)
const layoutTest = new LayoutTest(core)

const helperTest = new HelperTest(core)

const pageBuilderTest = new PageBuilderTest(core)
const rowBuilderTest = new RowBuilderTest(core)

describe('Inside', () => {
  /*
  * Core
  */
  coreTest.setupTest()
  const coreComponent = core.getApp()

  coreComponent.$mount()

  /*
  * Others
  */
  layoutTest.setupTest()

  helperTest.setupTest()

  pageBuilderTest.setupTest()
  rowBuilderTest.setupTest()
})
