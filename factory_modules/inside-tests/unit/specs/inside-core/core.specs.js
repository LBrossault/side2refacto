import InsideCore from '~insideCore'

export default class CoreTest {
  constructor (core) {
    this.core = core
  }

  setupTest () {
    describe('Inside Core', () => {
      /*
      * Check config files
      */
      describe('Config files', () => {
        it('Check conf.site.js', () => {
          expect(typeof process.env.GLOBAL).to.equal('object')
        })

        it('Check layout in conf', () => {
          expect(typeof process.env.GLOBAL.layout).to.equal('string')
        })
      })

      /*
      * Check all Inside Core builders
      */
      describe('Inside Core methods', () => {
        it('Instantiate Inside Core', () => {
          expect(this.core instanceof InsideCore).to.equal(true)
        })

        it('Create App', () => {
          expect(typeof this.core.getApp).to.equal('function')
        })

        it('Create PageBuilder', () => {
          expect(typeof this.core.getPageBuilder).to.equal('function')
        })

        it('Create Router Class', () => {
          expect(typeof this.core.getRouter).to.equal('function')
        })

        it('Create Routes', () => {
          expect(this.core.router.routes).to.have.length.above(0)
        })

        it('Create Vue Router', () => {
          expect(typeof this.core.router.router).to.equal('object')
        })

        it('Create Store', () => {
          expect(typeof this.core.store.store).to.equal('object')
        })

        // it('Create GraphQL Provider', () => {
        //   expect(typeof this.core.graphQlBuilder.apolloProvider).to.equal('object')
        // })

        it('Init Vue translations', () => {
          expect(typeof this.core.translations.I18N).to.equal('object')
        })
      })

      /*
      * Check if library return elements
      */
      describe('Inside Modules', () => {
        let insideModuleBuilder
        let insideLibrary

        it('Create Inside Module Builder', () => {
          insideModuleBuilder = this.core.setInsideModuleBuilder()

          expect(typeof insideModuleBuilder).to.equal('object')
        })

        it('Create Inside Library', () => {
          insideLibrary = insideModuleBuilder.getInsideLibrary()

          expect(typeof insideLibrary).to.equal('object')
        })

        it('Get Library Plugins', () => {
          expect(typeof insideLibrary.plugins).to.equal('object')
        })

        it('Get Library Store modules', () => {
          expect(typeof insideLibrary.storeModules).to.equal('object')
        })
      })

      /*
      * Check Module Displays
      */
      describe('Inside Module Displays', () => {
        let componentDispatcher = this.core.componentDispatcher

        it('Check Inside Layouts', () => {
          expect(typeof componentDispatcher.modules['InsideLayouts']).to.equal('object')
        })

        it('Check Inside Common', () => {
          expect(typeof componentDispatcher.modules['InsideCommon']).to.equal('object')
        })

        it('Check Inside UI', () => {
          expect(typeof componentDispatcher.modules['InsideUi']).to.equal('object')
        })

        it('Check Inside News', () => {
          expect(typeof componentDispatcher.modules['InsideNews']).to.equal('object')
        })

        it('Check Inside Events', () => {
          expect(typeof componentDispatcher.modules['InsideEvents']).to.equal('object')
        })

        it('Check Inside Socials', () => {
          expect(typeof componentDispatcher.modules['InsideSocials']).to.equal('object')
        })

        it('Get all displays in specific module - Error', () => {
          expect(componentDispatcher.getFullModule(null)).to.equal('error')
        })

        it('Get specific display - Error', () => {
          expect(componentDispatcher.getModuleDisplay(null, null)).to.equal('error')
        })
      })
    })
  }
}
