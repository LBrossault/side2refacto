import Vue from 'vue'

export default {
  name: 'inside-news',
  displays: {
    'capsule': Vue.component('capsule-news', () => import('./displays/CapsuleNews')),
    'full': Vue.component('full-news', () => import('./displays/FullNews')),
    'hero': Vue.component('hero-news', () => import('./displays/HeroNews')),
    'minimal': Vue.component('minimal-news', () => import('./displays/MinimalNews'))
  },
  config: {
    tabs: [
      {
        name: 'content',
        label: 'Content'
      },
      {
        name: 'display',
        label: 'Display'
      },
      {
        name: 'general',
        label: 'General'
      }
    ]
  }
}
