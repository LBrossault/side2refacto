import Vue from 'vue'

export default {
  name: 'inside-events',
  displays: {
    'capsule': Vue.component('capsule-events', () => import('./displays/CapsuleEvents'))
  },
  config: {
    tabs: [
      {
        name: 'content',
        label: 'Content'
      },
      {
        name: 'general',
        label: 'General'
      }
    ]
  }
}
