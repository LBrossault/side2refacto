/**
 * @file
 *
 * Index all builders.
 */

import Vue from 'vue'

/*
* Builders
*/

export default {
  RowBuilder: Vue.component('row-builder', () => import('./RowBuilder')),
  LayoutBuilder: Vue.component('layout-builder', () => import('./LayoutBuilder')),
  BlockBuilder: Vue.component('block-builder', () => import('./BlockBuilder'))
}
