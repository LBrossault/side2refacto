import Vue from 'vue'

export default {
  BlockTabContent: Vue.component('block-tab-content', () => import('./BlockTabContent')),
  BlockTabDisplay: Vue.component('block-tab-display', () => import('./BlockTabDisplay')),
  BlockTabGeneral: Vue.component('block-tab-general', () => import('./BlockTabGeneral'))
}
