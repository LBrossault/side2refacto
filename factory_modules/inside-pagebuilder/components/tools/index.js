/**
 * @file
 *
 * Index all tools.
 */

import Vue from 'vue'

export default {
  PageBuilderUi: Vue.component('page-builder-ui', () => import('./PageBuilderUI')),
  TooltipContent: Vue.component('tooltip-content', () => import('./TooltipContent'))
}
