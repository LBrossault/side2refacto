export default [
  'eq',
  'ne',
  'gt',
  'gte',
  'lt',
  'lte',
  'like'
]
