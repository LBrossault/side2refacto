module.exports = [
  {
    label: 'One column',
    nbCol: 1
  },
  {
    label: 'Two columns',
    nbCol: 2
  },
  {
    label: 'Three columns',
    nbCol: 3
  },
  {
    label: 'Four columns',
    nbCol: 4
  }
]
