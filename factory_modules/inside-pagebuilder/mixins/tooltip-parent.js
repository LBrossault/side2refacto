export default {
  data () {
    return {
      showTooltipContent: false
    }
  },
  methods: {
    tooltipAction (value) {
      this[value.action](value.data, value.tab)
    }
  }
}
