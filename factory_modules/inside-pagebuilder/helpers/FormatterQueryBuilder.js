export default class FormatterQueryBuilder {
  /*
  * Parse array
  */
  formattedArray (arr, type) {
    const formatted = type === 'filters' ? {} : []

    for (let y = 0; y < arr.length; y++) {
      if (type === 'filters') {
        if (typeof arr[y].value === 'object') {
          formatted[arr[y].key] = this.formattedArray(arr[y].value, type)
        } else {
          formatted[`${arr[y].key}:${arr[y].operation}`] = arr[y].value
        }
      } else {
        if (typeof arr[y].value === 'object' && arr[y].value) {
          formatted.push({
            [arr[y].key]: this.formattedArray(arr[y].value, type)
          })
        } else {
          formatted.push(arr[y].key)
        }
      }
    }

    return formatted
  }

  getURLContentType (contentTypes) {
    let contents = ''

    for (let y = 0; y < contentTypes.length; y++) {
      if (contents !== '') {
        contents = `${contents},${contentTypes[y]}`
      } else {
        contents = contentTypes[y]
      }
    }

    return contents
  }

  getURLFilters (filters, filtersDefault) {
    const allFilters = {}

    /*
    * Custom filters
    */
    for (let filter in filters) {
      if (filters[filter].value) {
        if (typeof filters[filter].value === 'object') {
          allFilters[filters[filter].key] = this.formattedArray(filters[filter].value, 'filters')
        } else {
          allFilters[`${filters[filter].key}:${filters[filter].operation}`] = filters[filter].value
        }
      }
    }

    /*
    * Default filters
    */
    for (let filter in filtersDefault) {
      if (filtersDefault[filter] && filter !== 'order') {
        if (filter === 'sort') {
          allFilters[filter] = `${filtersDefault[filter]}:${filtersDefault['order']}`
        } else {
          allFilters[filter] = filtersDefault[filter]
        }
      }
    }

    return allFilters
  }

  getURLFields (fields) {
    const allFields = []

    for (let field in fields) {
      if (typeof fields[field].value === 'object' && fields[field].value) {
        allFields.push({
          [fields[field].key]: this.formattedArray(fields[field].value, 'fields')
        })
      } else {
        allFields.push(fields[field].key)
      }
    }

    return allFields
  }
}
