import { PageBuilderComponents } from './components'

export default class PageBuilder {
  /*
  * Return all page builder components
  */
  getPageBuilderComponents () {
    return PageBuilderComponents
  }
}
