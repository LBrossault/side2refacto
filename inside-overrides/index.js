import Vue from 'vue'

export default {
  /*
  * Inside News
  */
  'capsule-news': Vue.component('capsule-news', () => import('./inside-news/CapsuleNews'))
}
