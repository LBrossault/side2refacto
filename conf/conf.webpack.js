const path = require('path')

module.exports = {
  isFactory: true,
  context: path.resolve(__dirname, '../'),
  customPath: path.resolve(__dirname, '../inside-customs'),
  aliases: {
    '~root': '../'
  },
  jarvisPort: 2337
}