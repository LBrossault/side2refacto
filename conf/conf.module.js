module.exports = {
  modules: {
    InsideCommon: require('../factory_modules/inside-library/components/common').default,
    InsideEvents: require('../factory_modules/inside-events').default,
    InsideLayouts: require('../factory_modules/inside-layouts').default,
    InsideNews: require('../factory_modules/inside-news').default,
    InsideUi: require('../factory_modules/inside-library/components/ui').default,
    InsideSocials: require('../factory_modules/inside-socials').default,
    InsideUsers: require('../factory_modules/inside-users').default,
    InsideCustoms: require('../inside-customs').default
  },
  vuePlugins: [

  ]
}
