import Vue from 'vue'

export default {
  name: 'inside-customs',
  displays: {
    'my-space': Vue.component('my-space-customs', () => import('./components/MySpaceCustoms'))
  },
  config: {
    tabs: [
      {
        name: 'general',
        label: 'General'
      }
    ]
  }
}
